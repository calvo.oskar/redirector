<?php

namespace Drupal\redirector\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\redirector\Helper\RedirectorHelper;

class BuildFileForm extends FormBase {
  
  
  /**
   * Getter method for Form ID.
   */
  public function getFormId() {
    return  'redirector_build_form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('This form let you to build a file with the redirections that can be used with proxys, servers or .htaccess file')
    ];
    
    $form['export_to'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => [
        'htaccess' => '.htaccess',
        'varnish' => 'Varnish',
        'haproxy' => 'Haproxy',
        'apache' => 'Apache2',
        'nginx' => 'Nginx'
      ]
    ];
  
    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement submitForm() method.
    $proxy = $form_state->getValue('export_to');
    $batch = [];
    $batch = $this->generateBatch($proxy);
    batch_set($batch);
  }
  
  private function generateBatch($proxy) {
    $list_redirects =  RedirectorHelper::getAllEntities();
    $wadus = '';
    foreach ($list_redirects as $redirection) {
      $wadus = '';
      $operations[] = [
        '\Drupal\redirector\Helper\RedirectorHelper::BuildRedirection',
        [$redirection, $proxy]
        
      ];
    }
    
    $batch = [
      'title' => t('Creating the redirection'),
      'operations' => $operations,
      'finished' => '\Drupal\redirector\Helper\RedirectorHelper::BuildFile'
    ];
    
    return $batch;
  }
}