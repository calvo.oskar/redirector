<?php

namespace Drupal\redirector\Helper;


class RedirectorPathAlias {
  
  static function getAllAlias(){
    $alias = \Drupal::service('path.alias_storage');
    return $alias->getAliasesForAdminListing([],NULL);
  }
}