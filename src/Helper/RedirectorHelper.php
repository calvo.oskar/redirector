<?php
namespace Drupal\redirector\Helper;


use Drupal\redirect\Entity\Redirect;

class RedirectorHelper {
  
  /**
   * @return array $all_entities
   */
  static function getAllEntities() {
    $query = \Drupal::entityQuery('redirect');
  
    $entity_ids = $query->execute();
    $all_entities = \Drupal::entityTypeManager()->getStorage('redirect')->loadMultiple($entity_ids);
    return $all_entities;
  }
  
  /**
   * @param \Drupal\redirect\Entity\Redirect $object
   * @param $proxy
   */
  static function BuildRedirection(Redirect $object, $proxy) {

    $status_code = $object->get('status_code')->getValue()[0]['value'];
    /*
    Array    (
      [path] => node/1
      [query] =>
    )
    */
    $redirect_source = $object->get('redirect_source')->getValue()[0];
    /*
    Array    (
      [uri] => internal:/node/2
      [title] =>
      [options] => Array        (        )
    )
    */
    $redirect_redirect = $object->get('redirect_redirect')->getValue()[0];
    
  }
  
  /**
   * @param $success
   * @param $results
   * @param $operations
   */
  static function BuildFile($success, $results, $operations) {
    if($success){
      drupal_set_message('wadus 25');
    }
  }
  
  private function htaccess($redirect_source, $redirect_redirect, $status_code) {
  
  }
}